class Game {
    constructor() {
        this.backgorund = null;
        this.weapon = null;
        this.selectedCharacter = null;
        this.cursors = null;
        this.enemies = null;
        this.laser = null;
        this.bombSound = null;
        this.fKey = null;
        this.numberOfAdditions = 0;
        this.explosions = null;
        this.selectedCharacterHealth = 100;
        this.livesText = null;

    }

    preload() {
        this.load.spritesheet('bullet', 'assets/bullets.png', 6, 2);
        this.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);

        this.load.image('bomb', 'assets/bomb.png');
        this.load.image('background', 'assets/sb-page.png');
        this.load.image('lawrence', 'assets/lawrence.png');
        this.load.image('nathan', 'assets/nathan.png');
        this.load.image('naresh', 'assets/naresh.png');
        this.load.image('turret', 'assets/turret.png');
        this.load.image('turret-background', 'assets/turret-background.png');


        this.load.audio('shots-sound', 'assets/sounds/M4A1_Single-Kibblesbob-8540445.mp3');
        this.load.audio('bomb-sound', 'assets/sounds/bomb.mp3');


        this.load.image('bug', 'assets/bug.png')
    }

    create() {
        this.laser = this.add.audio('shots-sound');
        this.laser.volume = 0.5;

        this.bombSound = this.add.audio('bomb-sound');

        this.fKey = this.input.keyboard.addKey(Phaser.Keyboard.F);
        this.sKey = this.input.keyboard.addKey(Phaser.Keyboard.S);

        this.sound.setDecodedCallback([this.laser, this.bombSound], this._start, this);
    }

    render() { }

    update() {
        if (this.selectedCharacter) {
            this.selectedCharacter.rotation = this.physics.arcade.angleToPointer(this.selectedCharacter);

            this.physics.arcade.overlap(this.selectedCharacter, this.bugs.bullets, (selectedCharacter, bullet) => {
                bullet.kill();

                this.selectedCharacterHealth -= 10;

                if (this.selectedCharacterHealth === 0) {
                    this.paused = true;
                    alert('You lose!!!');
                    location.reload();
                } else {
                    this._updatePlayerLives();
                }
            });

            this.physics.arcade.collide(this.bomb.bullets, this.bugs.bullets, (bullet, enemy) => {
                enemy.kill();
                bullet.kill();

                this._showExplosion(enemy);

                this.bombSound.play();
            }, null, this);

            this.physics.arcade.overlap(this.weapon.bullets, this.bugs.bullets, (bullet, enemy) => {
                enemy.kill();
                bullet.kill();

                this._showExplosion(enemy);
            }, null, this);

            this.game.world.wrap(this.selectedCharacter, 90);

            if (this.fKey.isDown) {
                this.bomb.fire();
            }

            if (this.sKey.isDown) {
                this.weapon.fire();
            }
        }
    }

    _start() {
        this.backgorund = this.add.tileSprite(0, 0, 1366, 727, 'background');
        this.physics.startSystem(Phaser.Physics.ARCADE);

        this.livesText = this.add.text(10, 10, `Player Lives: ${this.selectedCharacterHealth}`, { font: '34px Arial', fill: '#fff' });

        this.weapon = this.add.weapon(500, 'bullet');
        this.bomb = this.add.weapon(5, 'bomb');
        this.bugs = this.add.weapon(100, 'bug');

        this.turretBackground = this.add.sprite(880, 250, 'turret-background');
        this.turret = this.add.sprite(1000, 350, 'turret');
        this.turret.anchor.setTo(0.5, 0.5);
        this.turret.angle = - 45;

        this.weapon.setBulletFrames(0, 80, true, true);

        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weapon.bulletSpeed = 2000;
        this.weapon.fireRate = 30;

        this.bugs.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bugs.bulletSpeed = 180;
        this.bugs.autofire = true;
        this.bugs.bulletAngleVariance = 45;
        this.bugs.fireAngle = 180;
        this.bugs.fireLimit = 150;

        this.bomb.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bomb.bulletSpeed = 200;
        this.bomb.bulletAngleVariance = 40;

        this.selectedCharacter = this.add.sprite(200, 300, window.selectedCharacter);
        this.selectedCharacter.immovable = false;

        this.selectedCharacter.anchor.set(0.5);

        this.physics.arcade.enable(this.selectedCharacter);
        this.physics.arcade.enable(this.weapon);
        this.physics.arcade.enable(this.bugs);

        this.selectedCharacter.body.drag.set(200);
        this.selectedCharacter.body.maxVelocity.set(200);
        this.selectedCharacter.body.allowRotation = false;

        this.weapon.trackSprite(this.selectedCharacter, 0, -15, true);
        this.bomb.trackSprite(this.selectedCharacter, 0, -15, true);
        this.bugs.trackSprite(this.turret, -100, 10, false);

        this.cursors = this.input.keyboard.createCursorKeys();

        // this._addEnemies();
        this._addExplosions();

        this.add.tween(this.turret).to( { angle: 45 }, 1000, Phaser.Easing.Linear.None, true, 1000, 1000, true);

        this.weapon.onFire.add(() => {
            this.laser.play();
        });

        this.bugs.onFireLimit.add(() => {
            alert('You win, go to next level!!!');
            window.qaVsBugsGame.state.start('Game-Hard-Level');
        });
    }

    _showExplosion(target) {
        const explosion = this.explosions.getFirstExists(false);
        explosion.reset(target.body.x, target.body.y);
        explosion.play('kaboom', 30, false, true);
    }

    _addExplosions() {
        this.explosions = this.add.group();
        this.explosions.createMultiple(30, 'kaboom');
        this.explosions.forEach((invader) => {
            invader.anchor.x = 0.5;
            invader.anchor.y = 0.5;
            invader.animations.add('kaboom');
        }, this);
    }

    _updatePlayerLives() {
        this.livesText.text = `Player Lives: ${this.selectedCharacterHealth}`;
    }
}
