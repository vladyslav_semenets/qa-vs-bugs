window.onload = function () {
    window.qaVsBugsGame = new Phaser.Game(1366, 727, Phaser.AUTO, 'game');

    window.qaVsBugsGame.state.add('Game', Game, false);
    window.qaVsBugsGame.state.add('Game-Hard-Level', GameHardLevel, false);
    window.qaVsBugsGame.state.add('Menu', Menu, false);

    window.qaVsBugsGame.state.start('Menu');
}
