class GameHardLevel {
    constructor() {
        this.backgorund = null;
        this.selectedCharacter = null;
        this.cursors = null;
        this.enemies = null;
        this.laser = null;
        this.bombSound = null;
        this.fKey = null;
        this.numberOfAdditions = 0;
        this.boss = null;
        this.bossBomb = null;
        this.bossHealth = 100;
        this.selectedCharacterHealth = 100;
        this.livesText = null;
        this.bossLivesText = null;
        this.hearts = null;
        this.showedHeart = null;
        this.usedHearts = null;
    }

    preload() {
        this.load.spritesheet('bullet', 'assets/bullets.png', 6, 2);
        this.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);

        this.load.image('bomb', 'assets/bomb.png');
        this.load.image('background', 'assets/sb-page.png');
        this.load.image('lawrence', 'assets/lawrence.png');
        this.load.image('nathan', 'assets/nathan.png');
        this.load.image('naresh', 'assets/naresh.png');

        this.load.image('protection', 'assets/protection.png');

        this.load.audio('laser', 'assets/sounds/sf_laser_18.mp3');
        this.load.audio('bomb-sound', 'assets/sounds/bomb.mp3');

        this.load.image('bug', 'assets/bug.png');
        this.load.image('heart', 'assets/heart-small.png');

        this.load.image('bug-big', 'assets/big-bug.png');
    }

    create() {
        this.laser = this.add.audio('laser');

        this.laser.volume = 0.5;

        this.bombSound = this.add.audio('bomb-sound');

        this.fKey = this.input.keyboard.addKey(Phaser.Keyboard.F);

        this.sound.setDecodedCallback([this.laser, this.bombSound], this._start, this);

        this._startBounceTween();
    }

    update() {
        if (this.selectedCharacter) {
            if (this.boss) {
                this.physics.arcade.overlap(this.protections.children, this.bomb.bullets, (protection, bullet) => {
                    bullet.kill();
                    return false;
                }, null, this);

                this.physics.arcade.overlap(this.protections.children, this.bossBomb.bullets, (protection, bullet) => {
                    bullet.kill();
                    return false;
                }, null, this);

                this.physics.arcade.overlap(this.bomb.bullets, this.bossBomb.bullets, (playerBomb, bossBomb) => {
                    this.bombSound.play();

                    this._showExplosion(bossBomb);

                    bossBomb.kill();
                    playerBomb.kill();
                    return false;
                }, null, this);
            }

            if (this.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                this.selectedCharacter.x -= 4;
            } else if (this.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.selectedCharacter.x += 4;
            }

            if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
                this.selectedCharacter.y += 4;
            } else if (this.input.keyboard.isDown(Phaser.Keyboard.UP)) {
                this.selectedCharacter.y -= 4;
            }

            this.physics.arcade.overlap(this.selectedCharacter, this.hearts, (selectedCharacter, heart) => {
                heart.kill();
                this.usedHearts++;
                this.selectedCharacterHealth += 10;
                this._updatePlayerLives();
            });

            this.physics.arcade.overlap(this.bossBomb.bullets, this.selectedCharacter, (selectedCharacter, bossBomb) => {
                bossBomb.kill();

                this.selectedCharacterHealth -= 10;

                if (this.selectedCharacterHealth === 0) {
                    alert('You lose!!!');
                    location.reload();
                } else {
                    this._updatePlayerLives();
                }
            });

            this.physics.arcade.overlap(this.boss, this.bomb.bullets, (boss, bullet) => {
                bullet.kill();
                this.bombSound.play();

                this.bossHealth -= 5;

                if (this.bossHealth === 0) {
                    boss.kill();
                    this.bossBomb.active = false;
                    alert('You win!!! Let\'s kill the next boss');

                    setTimeout(() => {
                        this.boss.revive();
                        this.selectedCharacterHealth += 10;
                        this.bossHealth = 100;
                        this.usedHearts = 0;
                        this.bossBomb.active = true;

                        this._updateBossLives();
                        this._updatePlayerLives();
                    }, 3000)
                } else {
                    this._updateBossLives();
                    this._showExplosion(boss);
                }
            });

            this.physics.arcade.collide(this.selectedCharacter, this.boss, () => {
                alert('You lose!!!');
                location.reload();
            });

            this.game.world.wrap(this.boss, 90);
            this.game.world.wrap(this.selectedCharacter, 90);

            if (this.fKey.isDown) {
                this.bomb.fire();
            }
        }
    }

    _start() {
        this.physics.setBoundsToWorld();
        this.physics.startSystem(Phaser.Physics.ARCADE);

        this.backgorund = this.add.tileSprite(0, 0, 1366, 727, 'background');
        this.selectedCharacter = this.add.sprite(100, 200, window.selectedCharacter ?? 'lawrence');

        this.livesText = this.add.text(10, 10, `Player Lives: ${this.selectedCharacterHealth}`, { font: '34px Arial', fill: '#fff' });
        this.bossLivesText = this.add.text(10, 50, `Boss Lives: ${this.bossHealth}`, { font: '34px Arial', fill: '#fff' });

        this.boss = this.add.sprite(1000, 0, 'bug-big');

        this.bomb = this.add.weapon(10, 'bomb');

        this.bossBomb = this.add.weapon(1000, 'bug');

        this.bomb.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bomb.bulletSpeed = 800;

        this.bossBomb.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bossBomb.bulletSpeed = 500;
        this.bossBomb.autofire = true;
        this.bossBomb.bulletAngleVariance = 45;

        this.physics.arcade.enable(this.boss);
        this.physics.arcade.enable(this.selectedCharacter);
        this.physics.arcade.enable(this.bomb);
        this.physics.arcade.enable(this.bossBomb);


        this.selectedCharacter.width = 100;
        this.selectedCharacter.height = 100;

        this.bossBomb.fireAngle = 180;

        this.bossBomb.trackSprite(this.boss, 0, 100, false);

        this.bomb.trackSprite(this.selectedCharacter, 50, 50, true);

        this.selectedCharacter.immovable = false;
        this.boss.body.immovable = false;

        this.boss.width= 200;
        this.boss.height= 200;
        this.cursors = this.input.keyboard.createCursorKeys();
        this.selectedCharacter.body.collideWorldBounds = true;

        this._addProtections();
        this._addExplosions();
        this._startBounceTween();
        this._addHearts();

        setInterval(() => {
            if (this.selectedCharacterHealth < 90 && this.usedHearts !== 3 ||
                (this.selectedCharacterHealth < 20 && this.usedHearts === 3)
            ) {
                this.hearts.remove(this.showedHeart);
                this.showedHeart = this._showHeart();
            }
        }, 2000);
    }

    _startBounceTween() {
        const bounce = this.add.tween(this.boss);

        bounce.to({ y: this.world.height - this.boss?.height }, 2000, Phaser.Easing.Linear.None, true, 1000, 1000, true)
        bounce.start();
    }

    _updatePlayerLives() {
        this.livesText.text = `Player Lives: ${this.selectedCharacterHealth}`;
    }

    _updateBossLives() {
        this.bossLivesText.text = `Boss Lives: ${this.bossHealth}`;
    }

    _addProtections() {
        this.protections = this.add.group();

        const protectionFirst = this.protections.create(200, 100, 'protection');
        const protectionSecond = this.protections.create(200, 500, 'protection');

        protectionFirst.width = 120;
        protectionFirst.height = 120;

        protectionSecond.width = 120;
        protectionSecond.height = 120;

        this.physics.enable(protectionFirst, Phaser.Physics.ARCADE);
        this.physics.enable(protectionSecond, Phaser.Physics.ARCADE);

        this.protections.add(protectionFirst);
        this.protections.add(protectionSecond);
    }

    _showExplosion(target) {
        const explosion = this.explosions.getFirstExists(false);
        explosion.reset(target.body.x, target.body.y);
        explosion.play('kaboom', 30, false, true);
    }

    _showHeart() {
        const heart = this.hearts.getFirstExists(false);
        heart.reset(400, 400);

        return heart;
    }

    _addHearts() {
        this.hearts = this.add.group();
        this.hearts.createMultiple(100, 'heart');
        this.hearts.forEach((heart) => {
            heart.anchor.x = 0.5;
            heart.anchor.y = 0.5;
            heart.animations.add('heart');

            this.physics.enable(heart, Phaser.Physics.ARCADE);
        }, this);
    }

    _addExplosions() {
        this.explosions = this.add.group();
        this.explosions.createMultiple(1000, 'kaboom');
        this.explosions.forEach((invader) => {
            invader.anchor.x = 0.5;
            invader.anchor.y = 0.5;
            invader.animations.add('kaboom');
        }, this);
    }
}
