let game = new Phaser.Game(1366, 727, Phaser.AUTO, 'game', {
    preload: preload,
    create: create,
    render: render,
    update: update
});

let lawrence;
let weapon;
let cursors;
let fireButton;
let background;
let enemies

function preload() {
    this.load.spritesheet('bullet', 'assets/rgblaser.png', 4, 4);

    this.load.image('background', 'assets/sb-page.png');
    this.load.image('lawrence', 'assets/lawrence.png');
    this.load.image('bug', 'assets/bug.png')
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    background = game.add.tileSprite(0, 0, 1366, 727, 'background');

    weapon = this.add.weapon(40, 'bullet');
    weapon.setBulletFrames(0, 80, true);

    weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
    weapon.bulletSpeed = 800;
    // weapon.fireRate = 30;
    weapon.autofire = true;

    lawrence = this.add.sprite(400, 300, 'lawrence');

    lawrence.anchor.set(0.5);

    game.physics.arcade.enable(lawrence);
    game.physics.arcade.enable(weapon);


    lawrence.body.drag.set(200);
    lawrence.body.maxVelocity.set(200);
    lawrence.body.allowRotation = false;

    weapon.trackSprite(lawrence, 0, -15, true);
    cursors = this.input.keyboard.createCursorKeys();

    fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

 addEnemies();
}

function update() {
    lawrence.rotation = game.physics.arcade.angleToPointer(lawrence);
    game.physics.arcade.collide(weapon.bullets, enemies, function (bullet, enemy) {
        enemy.kill();

        let countAlive = 0;

        for (let i = 0; i < enemies.children.length; i++) {
            if (enemies.children[i].alive) {
                countAlive++;
            }
        }
        if (countAlive === 0) {
            addEnemies();
        }
    }, null, this);

    if (game.input.activePointer.isDown) {
        weapon.fire();
    }

    game.world.wrap(lawrence, 90);
}

function addEnemies() {
    enemies = game.add.group();

    for (let i = 0; i < 100; i++) {
        const enemy = enemies.create(683 + Math.random() * 683, Math.random() * 500, 'bug');
        game.physics.enable(enemy, Phaser.Physics.ARCADE);
        enemy.body.immovable = false;
        enemy.anchor.set(0.5);
        enemies.add(enemy);
    }

}

function render() {
    // weapon.debug();
}
