class Menu {
    constructor() {
        this.lawrence = null;
        this.nathan = null;
        this.naresh = null;
        this.backgoundSound = null;
    }

    preload () {
        this.load.image('lawrence', 'assets/lawrence.png');
        this.load.image('nathan', 'assets/nathan.png');
        this.load.image('naresh', 'assets/naresh.png');

        this.load.audio('backgound-sound', 'assets/sounds/backgound-sound.mp3');
    }

    create () {
        this.backgoundSound = this.add.audio('backgound-sound');
        this.backgoundSound.volume = 0.1;

        this.sound.setDecodedCallback([this.backgoundSound], this._start, this);
    }

    _start() {
        // this.backgoundSound.play();
        // this.backgoundSound.volume = 0.5;
        // this.backgoundSound.loopFull(1);

        this.add.text(300, 50, 'Please choose a QA Soldier', {
            font: '65px Arial',
            fill: 'white',
            align: 'center'
        });

        this.lawrence = this.add.sprite(100, 200, 'lawrence');
        this.nathan = this.add.sprite(500, 200, 'nathan');
        this.naresh = this.add.sprite(900, 200, 'naresh');

        this.lawrence.inputEnabled = true;
        this.nathan.inputEnabled = true;
        this.naresh.inputEnabled = true;

        this.lawrence.events.onInputDown.add(this._setCharacterAndStartGame('lawrence'), this);
        this.naresh.events.onInputDown.add(this._setCharacterAndStartGame('naresh'), this);
        this.nathan.events.onInputDown.add(this._setCharacterAndStartGame('nathan'), this);
    }

    _setCharacterAndStartGame(character) {
        return () => {
            window.selectedCharacter = character;
            window.qaVsBugsGame.state.start('Game');
        }
    }
}
